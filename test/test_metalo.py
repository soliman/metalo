"""Tests for Metalo.

Copyright (C) 2023 Sahar.Aghakhani@inria.fr and Sylvain.Soliman@inria.fr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from contextlib import redirect_stdout
from io import StringIO
from math import isclose
from unittest.mock import patch

from metalo.metalo import main


def test_metalo_works_on_caf_files():
    """Check if the results on CAF are ok."""
    with patch(
        "sys.argv",
        [
            "metalo",
            "-i",
            "test/Breast_CAF_initial_conditions.csv",
            "test/CAF-map_V2_MitoCore_ID.xml",
            "test/mitocore_v1.01.xml",
        ],
    ):
        with redirect_stdout(StringIO()) as f:
            main()
    for line in f.getvalue().splitlines():
        if line.startswith("The proportion"):
            if line.find("control") >= 0:
                control = float(line.split()[-1][:-1])
            elif line.find("trap-spaces") >= 0:
                trap = float(line.split()[-1][:-1])

    assert isclose(control, 0.0405)
    assert isclose(trap, 0.8505)
